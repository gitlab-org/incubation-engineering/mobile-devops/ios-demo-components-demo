# iOS Demo - Components Demo

## Background

This project is a fork of the [iOS Demo](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/demo-projects/ios-demo) reference project intended to showcase the upcoming [CI/CD Catalog Component](https://docs.gitlab.com/ee/architecture/blueprints/ci_pipeline_components/) features.

This project provides the same build, sign, and release pipeline as the original demo project using only prebuilt components. The eliminates the need for any fastlane configuration in the project itself.

## Prerequisites

This demo assumes that the project's code signing files have already been uploaded to [Project-level Secure Files](https://docs.gitlab.com/ee/ci/secure_files/) and that the [Apple App Store Integration](https://docs.gitlab.com/ee/user/project/integrations/apple_app_store.html) has been activated for the project. 

**Project-level Secure Files**

<img src="/uploads/cea695e4dce9ed179ca4d8d521bb1d11/Xnapper-2023-04-12-16.03.38.png" width="60%">

**Apple App Store Integration**

<img src="/uploads/e62840296b8e7b8cb6f3c034257c9e2e/Xnapper-2023-04-12-16.02.24.png" width="60%">

## iOS Components

With the prerequisites met, the pipeline is enabled by loading and configuring the components from the [iOS Components POC](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/ios-components-poc) project. See the components readme for usage and configuration options.

The [.gitlab-ci.yml](https://gitlab.com/gitlab-org/incubation-engineering/mobile-devops/ios-demo-components-demo/-/blob/main/.gitlab-ci.yml) file shows a functional example for the demo project.

* The `include` section adds and configures the components
* The `stages` section sets up the stages for the pipeline. Stage names can be configured as component inputs - this example uses defaults.
* The `default` section sets the `image` and `tags` needed to run these jobs on [macOS SaaS Runners](https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html). It also adds a `before_script` to update the fastlane gem.

## Pipeline

This configuration produces a pipeline with two stages. The first stage will build and sign the app and create an IPA artifact file. The second stage will upload that artifact as a release to Test Flight. The release stages is manually triggered and only runs on the main branch.

<img src="/uploads/5b1d2c4c403089dee4115df13c79debc/image.png" width="60%">
